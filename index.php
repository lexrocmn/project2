<html>
<?php include("message.php");?>
<head>
    <title>Restaurant Oud Leusden :: Home</title>
    <link type="text/css" rel="stylesheet" href="css/styles.css">
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script type="text/javascript" href="js/script.js"></script>
    <script type="text/javascript" src="js/snowstorm.js"></script>
</head>
<body>
<div class="background">
<div style="margin-top: 50px;" class="main">
    <p class="message"><?php echo $row['message']; ?></p>
    <nav class="nav">
        <ul class="navul-links">
            <li><a class="menulinks active" href="#">Home</a></li> 
            <li><a class="menulinks" href="restaurant.html">Restaurant</a></li>
            <li><a class="menulinks" href="fotos.html">Fotos</a></li>
            <li><a class="menulinks" href="actueel.php">Actueel</a></li>
            <li><a class="menulinks" href="recensie.php">Recensie</a></li>
            <li><a class="menulinks" href="contact.html">Contact</a></li>
            <li class="coral"><a class=" dropdown-button coral menulinks" data-activates='dropdown1'>Reserveren</a></li>
             <ul id='dropdown1' class='dropdown-content'>
                <li><a style="color:lightcoral;" href="reseveren.html">Restaurant</a></li>
                <li><a style="color:lightcoral;" href="reseveren.html">Zaal</a></li>
            </ul>
        </ul>
    </nav>
    <div class="header">
        <img class="header-image" src="http://www.theconnaught.co.uk/Sites/Connaught%20Hotel/library/images/Contentpage%20Header%20Image-Connaught%20hotel%20restuarant%20header-1.jpg">
    </div>
    <div class="row">
        <div class="artikel center">
            <h1 class="tie">Restaurant Oud Leusden</h1>
            <p>The place where you can get obese</p>
        </div>
    </div>
      <div class="row">
        <div class="col s4 m4">
          <div class="card">
            <div class="card-image">
              <img src="https://i.hungrygowhere.com/cms/2c/80/d9/3e/35673/2c80d93e35673_566x424_fillbg_484d3266c7.jpg">
              <span class="card-title">Naam gerecht</span>
            </div>
            <div class="card-content">
              <p>Hier komen gerechten te staan en de beschrijving wat erin zit en hoe het is gemaakt. Ook algergie informatie
             Nog meer tekst komt hier zo kan je uren doorgaan</p>
            </div>
            <div class="card-action">
                <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Deze functie is nog in de maak">Meer informatie</a>         
            </div>
          </div>
        </div>
        <div class="col s4 m4">
          <div class="card">
            <div class="card-image">
              <img src="https://i.hungrygowhere.com/cms/2c/80/d9/3e/35673/2c80d93e35673_566x424_fillbg_484d3266c7.jpg">
              <span class="card-title">Naam gerecht</span>
            </div>
            <div class="card-content">
              <p>Hier komen gerechten te staan en de beschrijving wat erin zit en hoe het is gemaakt. Ook algergie informatie
             Nog meer tekst komt hier zo kan je uren doorgaan</p>
            </div>
            <div class="card-action">
                <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Deze functie is nog in de maak">Meer informatie</a>         
            </div>
          </div>
        </div>
        <div class="col s4 m4">
          <div class="card">
            <div class="card-image">
              <img src="https://i.hungrygowhere.com/cms/2c/80/d9/3e/35673/2c80d93e35673_566x424_fillbg_484d3266c7.jpg">
              <span class="card-title">Naam gerecht</span>
            </div>
            <div class="card-content">
              <p>Hier komen gerechten te staan en de beschrijving wat erin zit en hoe het is gemaakt. Ook algergie informatie
             Nog meer tekst komt hier zo kan je uren doorgaan</p>
            </div>
            <div class="card-action">
                <a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Deze functie is nog in de maak">Meer informatie</a>         
            </div>
          </div>
        </div>
      </div>
    </div>
<footer class="page-footer main">
    <div class="container">
        <div class="row">
          <div class="col l6 s4">
            <h5 class="white-text">Restaurant Oud Leusden</h5>
            <p class="grey-text text-lighten-4">Aan de rand van Amersfoort, vlakbij de natuurrijke Leusderheide vindt u Restaurant Oud Leusden.</p>
          </div>
          <div class="col l4 offset-l2 s4">
            <h5 class="white-text">Handige links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3 " href="http://www.restaurantoudleusden.nl/~tolhuysnl/uploads/2/files/brochurepdf18.pdf">Menu Kaart</a></li>
              <li><a class="grey-text text-lighten-3 " href="#!">Vacatures</a></li>
              <li><a class="grey-text text-lighten-3 " href="#!">Info</a></li>
              <li><a class="grey-text text-lighten-3 " href="#!">Contact</a></li>
              <li><a class="grey-text text-lighten-3" href="login.php">Beheer</a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        <a target="_blank" href="https://www.twitter.com">"<img style="width:20px; height:20px;" src="https://cdn1.iconfinder.com/data/icons/logotypes/32/circle-twitter-512.png"></a>
        <a target="_blank" href="https://www.facebook.com">"<img style="width:25px; height:25px;" src="https://www.seeklogo.net/wp-content/uploads/2016/09/facebook-icon-preview-200x200.png"></a>
        © 2017 all rights reserved / mLo Media group.
        <a class="grey-text text-lighten-4 right" href="#!">mLo</a>
        </div>
      </div>
    </footer>
</div>
</body>
</html>