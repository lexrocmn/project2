<?php
function getNewsList(){
	
   $fileList = array();
   
	if ($handle = opendir("admin/news")) {
		while ($file = readdir($handle))  {
		    if (!is_dir($file)) {
		       $fileList[] = $file;
      	}
		}
	}	
	
	rsort($fileList);
	
	return $fileList;
}

?>
<html>
<head>
    <title>Restaurant Oud Leusden :: Home</title>
    <link type="text/css" rel="stylesheet" href="admin/style/style.css">
    <link type="text/css" rel="stylesheet" href="css/styles.css">
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script type="text/javascript" href="js/script.js"></script>
    <script type="text/javascript" src="js/snowstorm.js"></script>
</head>
<body>
<div class="background">
<div style="margin-top: 50px;" class="main">
    <nav class="nav">
        <ul class="navul-links">
            <li><a class="menulinks active" href="index.php">Home</a></li> 
            <li><a class="menulinks" href="restaurant.html">Restaurant</a></li>
            <li><a class="menulinks" href="fotos.html">Fotos</a></li>
            <li><a class="menulinks" href="actueel.php">Actueel</a></li>
            <li><a class="menulinks" href="recensie.php">Recensie</a></li>           
            <li><a class="menulinks" href="contact.html">Contact</a></li>
            <li class="coral"><a class=" dropdown-button coral menulinks" data-activates='dropdown1'>Reserveren</a></li>
             <ul id='dropdown1' class='dropdown-content'>
                <li><a style="color:lightcoral;" href="reseveren.html">Restaurant</a></li>
                <li><a style="color:lightcoral;" href="reseveren.html">Zaal</a></li>
            </ul>
        </ul>
    </nav>
     <div class="header">
        <img class="header-image-sub" src="http://booking-hotel-golf.falgos.com/public/pictures/header_restaurant-catalonia.jpg">
    </div>
    <div class="row">
    <div class="col s10 offset-s1">
  <div id="main">
    <div id="caption">Actueele informatie van Restaurant Oud Leusden</div>
    <table width="100%">
    <?php
    
      $list = getNewsList();
      foreach ($list as $value) {
      	$newsData = file("admin/news/".$value);
      	$newsTitle  = $newsData[0];
         $submitDate = $newsData[1];	
         unset ($newsData['0']);
         unset ($newsData['1']);
      	
         $newsContent = "";
         foreach ($newsData as $value) {
    	       $newsContent .= $value;
         }
      	
      	echo "<tr><th align='left'>$newsTitle</th><th align='right'>$submitDate</th></tr>";
      	echo "<tr><td colspan='2'>".$newsContent."<br/><hr size='1'/></td></tr>";
      }
    ?>
    </table>
	 <div id="source">Nieuws systeem v 1.0 mLo Media group</div>
  </div>
</div>
</div>
</div>

<footer class="page-footer main">
    <div class="container">
        <div class="row">
          <div class="col l6 s4">
            <h5 class="white-text">Restaurant Oud Leusden</h5>
            <p class="grey-text text-lighten-4">Aan de rand van Amersfoort, vlakbij de natuurrijke Leusderheide vindt u Restaurant Oud Leusden.</p>
          </div>
          <div class="col l4 offset-l2 s4">
            <h5 class="white-text">Handige links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3 " href="http://www.restaurantoudleusden.nl/~tolhuysnl/uploads/2/files/brochurepdf18.pdf">Menu Kaart</a></li>
              <li><a class="grey-text text-lighten-3 " href="#!"></a>Vacatures </li>
              <li><a class="grey-text text-lighten-3 " href="#!"></a>Info</li>
              <li><a class="grey-text text-lighten-3 " href="#!"></a>Contact</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-copyright">
        <div class="container">
        <a target="_blank" href="https://www.twitter.com">"<img style="width:20px; height:20px;" src="https://cdn1.iconfinder.com/data/icons/logotypes/32/circle-twitter-512.png"></a>
        <a target="_blank" href="https://www.facebook.com">"<img style="width:25px; height:25px;" src="https://www.seeklogo.net/wp-content/uploads/2016/09/facebook-icon-preview-200x200.png"></a>
        © 2017 all rights reserved / mLo Media group.
        <a class="grey-text text-lighten-4 right" href="#!">mLo</a>
        </div>
      </div>
    </footer>
    </div>
</body>


