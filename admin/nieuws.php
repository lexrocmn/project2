<?php
session_start();
include("../auth.php");
include("reserveringen.php");
if (!isset($_POST['submit'])) {

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html>
<head>
   <title>Restaurant Oud Leusden :: Nieuws</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
   <script language="javascript" type="text/javascript" src="js/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	theme_advanced_buttons3 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",

});
</script>
</head>

<body>
<div style="margin-top:20px;" class="main-admin background">
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Beheerders paneel</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="admin.php">Admin</a></li>
        <li><a href="nieuws.php">Nieuws plaatsen</a></li>
        <li><a href="res.php">Reserveringen</a></li>
        <li><a href="../logout.php">Log uit</a></li>
      </ul>
    </div>
  </nav>
        
  <div id="adminNieuws">
      <form class="adminNieuws" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">Nieuws title:<br/>
          <input type="text" name="title" size="40"/><br/><br/>Inhoud:<br/>
          <textarea name="newstext" rows="15" cols="67"></textarea><br/>
          <input class="btn" type="submit" name="submit" value="Plaats" />
      </form> 
    </div>
</body>   

<?php } else {
   $newsTitel   = isset($_POST['title']) ? $_POST['title'] : 'Geen Title';
   $submitDate  = date('Y-m-d g:i:s A');
   $newsContent = isset($_POST['newstext']) ? $_POST['newstext'] : 'Geen text';
   
   $filename = date('YmdHis');
   $f = fopen('news/'.$filename.".txt","w+");         
   fwrite($f,$newsTitel."\n");
   fwrite($f,$submitDate."\n");
   fwrite($f,$newsContent."\n");
   fclose($f);

   header('Location: nieuws.php');   
}
?>