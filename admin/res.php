<?php
session_start();
include("../auth.php");
include("reserveringen.php");
?>
<html>
<head>
   <title>Restaurant Oud Leusden :: Admin</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
</head>

<body>
<div style="margin-top:20px;" class="main-admin background">
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Beheerders paneel</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="admin.php">Admin</a></li>
        <li><a href="nieuws.php">Nieuws plaatsen</a></li>
        <li><a href="res.php">Reserveringen</a></li>
        <li><a href="../logout.php">Log uit</a></li>
      </ul>
    </div>
  </nav>
<div class="adminReservering">
    <h3>Recentste reservering</h3>
    <ul>
        <li><?php echo $row['id']; ?></li>
        <li><?php echo $row['datum']; ?></li>
        <li><?php echo $row['voornaam']; ?></li>
        <li><?php echo $row['achternaam']; ?></li>
        <li><?php echo $row['optie']; ?></li>
        <li><?php echo $row['email']; ?></li>
    </ul>   
</div>
</div>
</body>   
</html>