<?php
session_start();
include("../auth.php");
include("reserveringen.php");
?>
<html>
<head>
   <title>Restaurant Oud Leusden :: Admin</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
</head>

<body>
<div style="margin-top:20px;" class="main-admin background">
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Beheerders paneel</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="admin.php">Admin</a></li>
        <li><a href="nieuws.php">Nieuws plaatsen</a></li>
        <li><a href="res.php">Reserveringen</a></li>
        <li><a href="../logout.php">Log uit</a></li>
      </ul>
    </div>
  </nav>
<div class="adminMessage">
    <form action="message.php" method="post">
        <p>
            <label for="firstName">Bericht van de dag</label>
            <input type="text" name="message" id="message">
        </p>
        <input class="btn" type="submit" value="Plaats">
    </form>
</div>
</div>
</body>   
</html>